package ru.t1consulting.vmironova.tm.api.service;

import ru.t1consulting.vmironova.tm.enumerated.Sort;
import ru.t1consulting.vmironova.tm.enumerated.Status;
import ru.t1consulting.vmironova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAll(Sort sort);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    List<Task> findAllByProjectId(String projectId);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    void clear();

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
